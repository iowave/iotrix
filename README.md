# Iotrix


### Iotrix is an Android application designed to control P10 display and Led Strips on advertisement board over wifi.

![iotrix](image/iotrixbanner.png) 



## Watch video 

[![IMAGE ALT TEXT HERE](image/IMG_2136vjh.jpg)](https://www.youtube.com/watch?v=ToyvMSfwnVA)


APP LINK

https://play.google.com/store/apps/details?id=com.smartadvertisement.meohm.iotrix

Features.

The Iotrix app is packed full of features like

### Led Matrix Features:
# 
1. Single screen Blink mode
2. Double screen Blink mode
3. Simple Message mode
4. Scrolling Message mode
5. Split Display mode
6. Temperature mode
7. Counter mode
8. Easy Brightness control
9. Three Mode Display control
10. Switch on/off display
11. Control the Display size from 32 * 16 to 64 * 64.

### Light Strip Features:
# 
1. Two Led Strip Control
2. Blink mode
3. Fade mode
4. Alternate light mode
5. Brightness control
6. Speed control

### HACKSTER PROJECT DETAILS

https://www.hackster.io/parmarravi950/iotrix-smart-advertisement-led-board-cbfff5


### Hardware Supported 

![hardware](image/hardware.jpg) 


- Esp8266
- Arduino



