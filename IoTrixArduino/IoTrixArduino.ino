/*

  P10 LED matrix(2),RTC module ,Arduino Nano, ESP8266 01,led Strip (Green,White)

  pin mapping
  D0  -------NOT USE---[X]
  D1  -------NOT USE---[X]
  D2  ----------------- ESP tx
  D3~  ---------------- DSB1820 TEMP SESNOR
  D4   ---------------- ESP RX
  D5~  ------NOT USE--- [X]
  D6~  ---------------- A
  D7  ----------------- B
  D8  ----------------- SCK
  D9~ ----------------- OE
  D10~ ------NOT USE--- [X]
  D11~ ---------------- R
  D12 ----------------- ESP TX
  D13 ----------------- CLK
  A4  ----------------- RTC SDA
  A5  ----------------- RTC SCL
  A3  ------NOT USE--- [X]
  A2  ------NOT USE--- [X]
  A1  ------NOT USE--- [X]
  A0  ------NOT USE--- [X]
  Functions
  1.)Blink effect,Fading effect,ON/OFF mode led strip
  2.)RTC Date,time,day
  3.)Scrolling Message
  4.)Support mobile App for more dynamic control
*/
#include <SPI.h>        //SPI.h must be included as DMD is written by SPI (the IDE complains otherwise)
#include <DMD.h>
#include <TimerOne.h>
#include "arial_8.h"
#include "arialRounded_9.h"
#include "arial_11.h"
#include "arial_13.h"
#include "Arial_black_16.h"
#include "Droid_Sans_12.h"
#include "Droid_Sans_16.h"
#include "comicSans_14.h"
#include "SystemFont5x7.h"
#include "angka_2.h"
#include "OCRA.h"
#include "calibri9.h"
#include "mvboli.h"
#include "jamfont.h"
#include "seorgeScript_10.h"
#include <Wire.h>
#include <OneWire.h>
#include "RTClib.h"
OneWire  ds(3);
//Fire up the DMD library as dmd
#define DISPLAYS_ACROSS 1
#define DISPLAYS_DOWN 1
DMD dmd(DISPLAYS_ACROSS, DISPLAYS_DOWN);
int x, y;
int i = 0;
RTC_DS1307 RTC;
char Timebuf[10];
char Datebuf[10];
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

/*--------------------------------------------------------------------------------------
  Interrupt handler for Timer1 (TimerOne) driven DMD refresh scanning, this gets
  called at the period set in Timer1.initialize();
  --------------------------------------------------------------------------------------*/
void ScanDMD()
{
  dmd.scanDisplayBySPI();
}

/*--------------------------------------------------------------------------------------
  setup
  Called by the Arduino architecture before the main loop begins
  --------------------------------------------------------------------------------------*/
void setup()
{
  //initialize TimerOne's interrupt/CPU usage used to scan and refresh the display
  Timer1.initialize( 5000 );           //period in microseconds to call ScanDMD. Anything longer than 5000 (5ms) and you can see flicker.
  Timer1.attachInterrupt( ScanDMD );   //attach the Timer1 interrupt to ScanDMD which goes to dmd.scanDisplayBySPI()
  dmd.clearScreen( true );
  Serial.begin(9600);
  Wire.begin();
  RTC.begin();
  if (! RTC.isrunning()) {
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
  dmd.setBrightness(255);
}

/*--------------------------------------------------------------------------------------
  loop
  Arduino architecture main loop
  --------------------------------------------------------------------------------------*/
void loop()
{
  RunDefault();
}

void RunDefault() {
  scrollSingleMessage("+ METRO CHEMIST + Experienced Since 1983 +", 43, 30, 0, 0, comicSans_14);
  Temp(SystemFont5x7);
  staticSingleMessage(Time(), 10, 5000, 2, 0, 0, OCRA);
  splitDualMessage("METRO", "+CHEMIST+", 5, 10, 50, 2, 1, 0, SystemFont5x7, 1, 0);
  scrollSingleMessage("+ Skin Care + Baby Care + Vitamins + Ayurvedic + ", 50, 30, 0, 0, Arial_Black_16);
  scrollSingleMessage("+GENERIC DRUGS+ ", 15, 30, 0, 0, comicSans_14);
  staticSingleMessage(Time(), 10, 5000, 2, 0, 0, OCRA);
  splitDualMessage("METRO", "HOME DELIVERY:28463547", 5, 23, 50, 2, 1, 0, SystemFont5x7, 1, 0);
}
char* Time() {
  DateTime now = RTC.now();
  sprintf(Timebuf, "%02d:%02d", now.hour(), now.minute());
  return Timebuf;
}
char* Date() {
  DateTime now = RTC.now();
  sprintf(Datebuf, "%02d/%02d/%02d", now.day(), now.month(), now.year());
  return Datebuf;
}
char* DayofWeek() {
  DateTime now = RTC.now();
  return daysOfTheWeek[now.dayOfTheWeek()];
}
void scrollSingleMessage(char Mymsg[], int Mlength, int Mspeed, int AnimDirec, int InverseType, int MFont) {
  dmd.clearScreen(true);
  dmd.selectFont(MFont);
  dmd.drawMarquee(Mymsg, Mlength, 32, 0);
  long start = millis();
  long timer = start;
  boolean ret = false;
  while (!ret) {
    if ((timer + Mspeed) < millis()) {
      ret = dmd.stepMarquee(-1, 0);
      timer = millis();
    }
  }
}

void staticSingleMessage(char Mymsg[], int Mlength, int StaticTime, int x, int y, int InverseType, int MFont) {
  dmd.clearScreen(true);
  dmd.selectFont(MFont);
  dmd.drawString(x - 1, y - 1, Mymsg, Mlength, GRAPHICS_NORMAL);
  delay(StaticTime);
}
void splitDualMessage(char message1[], char message2[], int ML1, int ML2, int M1speed, int Scounter, int mode, int InverseType, int MFont, int xdis, int ydis) {
  dmd.clearScreen(true);
  dmd.selectFont(MFont);
  boolean ret = false;
  int n;
  unsigned long time;
  if (mode == 0) {
    dmd.drawString(xdis, ydis, message1, ML1, GRAPHICS_NORMAL); // the stationary string
    dmd.drawMarquee(message2, ML2, 32, 0); // set up the marquee
    time = millis();
    n = 0;
    while (n < Scounter) {
      while (!ret) {
        if ((time + M1speed) < millis()) {
          ret = dmd.stepSplitMarquee(0, 7); // parameters are the top & bottom rows to be scrolled
          time = millis();
        }
      }
      ret = false;
      n++;
    }
    dmd.clearScreen( true );
  }
  else if (mode == 1) {
    dmd.drawString(xdis, ydis, message1, ML1, GRAPHICS_NORMAL); // the stationary string
    dmd.drawMarquee(message2, ML2, 32, 8); // set up the marquee
    time = millis();
    n = 0;
    while (n < Scounter) {
      while (!ret) {
        if ((time + M1speed) < millis()) {
          ret = dmd.stepSplitMarquee(8, 15); // parameters are the top & bottom rows to be scrolled
          time = millis();
        }
      }
      ret = false;
      n++;
    }
    dmd.clearScreen( true );
  }
  else if (mode == 2) {
    dmd.drawString(0, -4, "vvvvvvvvvvv", 11, GRAPHICS_NORMAL); // note the position is above a single DMD so
    dmd.drawString(0, 13, "^^^^^^^^^^^", 11, GRAPHICS_NORMAL); // and this is too far down a single DMD so
    dmd.drawMarquee(message2, ML2, 32, 5);
    time = millis();
    n = 0;
    while (n < Scounter) {
      while (!ret) {
        if ((time + M1speed) < millis()) {
          ret = dmd.stepSplitMarquee(5, 11); // parameters are the top & bottom rows to be scrolled
          time = millis();
        }
      }
      ret = false;
      n++;
    }
    dmd.clearScreen( true );
  }
}
void BLinkSingleMessage(char message[], int Mlength, int xdir, int ydir, int Bspeed, int BCount, int MFont) {
  dmd.clearScreen(true);
  dmd.selectFont(MFont);
  for (int i = 0; i < BCount; i++) {
    for (byte x = 0; x < DISPLAYS_ACROSS; x++) {
      for (byte y = 0; y < DISPLAYS_DOWN; y++) {
        dmd.drawString(xdir + (32 * x), ydir - 1 + (16 * y), message, Mlength, GRAPHICS_OR );
        delay(Bspeed);
        dmd.drawString(xdir + (32 * x), ydir - 1 + (16 * y), message, Mlength, GRAPHICS_NOR );
        delay(Bspeed);
      }
    }
  }
}

void BLinkDoubleMessage(char message1[], char message2[], int ML1, int ML2, int xdir1, int ydir1, int xdir2, int ydir2, int Bspeed, int BCount, int MFont) {
  dmd.clearScreen(true);
  dmd.selectFont(MFont);
  for (int i = 0; i < BCount; i++) {
    for (byte x = 0; x < DISPLAYS_ACROSS; x++) {
      for (byte y = 0; y < DISPLAYS_DOWN; y++) {
        dmd.drawString(xdir1 + (32 * x), ydir1 - 1 + (16 * y), message1, ML1, GRAPHICS_OR );
        dmd.drawString(xdir2 + (32 * x), ydir2 - 1 + (16 * y), message2, ML2, GRAPHICS_OR );
        delay(Bspeed);
        dmd.drawString(xdir1 + (32 * x), ydir1 - 1 + (16 * y), message1, ML1, GRAPHICS_NOR );
        dmd.drawString(xdir2 + (32 * x), ydir2 - 1 + (16 * y), message2, ML2, GRAPHICS_NOR );

        delay(Bspeed);
      }
    }
  }
}
void Temp(int mFont) {
  byte i;
  byte present = 0;
  byte type_s;
  byte data[12];
  byte addr[8];
  float celsius, fahrenheit;

  ds.search(addr);
  delay(250);
  ds.reset();
  ds.select(addr);
  ds.write(0x44, 1);        // start conversion, with parasite power on at the end

  delay(750);     // maybe 750ms is enough for correct start up temp, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.

  present = ds.reset();
  ds.select(addr);
  ds.write(0xBE);         // Read Scratchpad

  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
  }

  // Convert the data to actual temperature
  // because the result is a 16 bit signed integer, it should
  // be stored to an "int16_t" type, which is always 16 bits
  // even when compiled on a 32 bit processor.
  int16_t raw = (data[1] << 8) | data[0];
  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (data[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } else {
    byte cfg = (data[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
    //// default is 12 bit resolution, 750 ms conversion time
  }
  celsius = raw / 16.0;
  fahrenheit = celsius * 1.8 + 32.0;
  celsius = celsius + 1; //fine tune temp here if out
  dmd.clearScreen(true);
  float t = celsius; // Temp Fine tune.
  //char str_buf[20];
  //snprintf(str_buf, 20, "Temperature %d.%d" "*C", (int)t, (int)(t * 100) - (int)t * 100); // string with degree C
  dmd.selectFont(mFont);
  dmd.drawString( 5 + (32 * x), 0 + (16 * y), "TEMP", 4, GRAPHICS_NORMAL);
  char str2_buf[20];
  snprintf(str2_buf, 20, "%d.%1d ", (int)t, (int)(t * 100) - (int)t * 100); // string with degree C
  Serial.println(str2_buf);
  dmd.drawString( 0 + (32 * x), 8 + (16 * y), str2_buf, 5, GRAPHICS_NORMAL);
  delay(5000);
}
