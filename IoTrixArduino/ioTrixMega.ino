/*

  P10 LED matrix(2),RTC module ,Arduino Mega , ESP8266 07,led Strip (Green,White)
  Arduino Mega pin mapping

  D0  -------NOT USE---[X]
  D1  -------NOT USE---[X]
  D2  -------NOT USE---[X]
  D3~  ---------------- DSB1820 TEMP SESNOR
  D4   -------NOT USE---[X]
  D5~  ------NOT USE--- [X]
  D6~  ---------------- A
  D7  ----------------- B
  D8  ----------------- SCK
  D9~ ----------------- OE
  D10~ ------NOT USE--- [X]
  D11~ ---------------- R
  D12  -------NOT USE---[X]
  D13 ----------------- CLK
  A4  ----------------- RTC SDA
  A5  ----------------- RTC SCL
  A3  ------NOT USE--- [X]
  A2  ------NOT USE--- [X]
  A1  ------NOT USE--- [X]
  A0  ------NOT USE--- [X]

  D18  ----------------- ESP RX
  D19 ------------------ ESP  TX
  Functions
  1.)Blink effect,Fading effect,ON/OFF mode led strip.
  2.)RTC Date,time,day,temperature mode.
  3.)Support mobile App for more dynamic control.
  4.)Widgets for custom control over app
     a)Scroll
     b)Single message
     c)split
     d)Blink double
     e)Blink Single
     f)Temperature Mode
     g)Counter
     h)Date/Time Mode
  5.)Led matrix is supported with 9 fonts.
  6.)Support for wifi mode and AP mode.
  7.)Retains all the data when the board gets restarted.

  Custom mode JSON PATTERN

  {
  "Custom": [7,8],
    "DATA":[[1] , [0]]
    }

*/

#include <EEPROM.h>
#include <SPI.h>
#include <DMD.h>
#include <TimerOne.h>
#include <Wire.h>
#include <OneWire.h>
#include "floatToString.h"
#include <ArduinoJson.h>
#include "RTClib.h"
#include "arial_8.h"
#include "arialRounded_9.h"
#include "arial_11.h"
#include "arial_13.h"
#include "Arial_black_16.h"
#include "Droid_Sans_12.h"
#include "Droid_Sans_16.h"
#include "comicSans_14.h"
#include "SystemFont5x7.h"
#include "angka_2.h"
#include "OCRA.h"
#include "calibri9.h"
#include "mvboli.h"
#include "jamfont.h"
#include "seorgeScript_10.h"

OneWire  ds(3);
RTC_DS1307 RTC;
char Timebuf[10];
char Datebuf[10];
char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

boolean a, b = false;
boolean Scroll = true;
boolean SplitState = false;
boolean CustomTrigger = false;

String Jsondata;
String custData;

int cDataSize;
int customJson[8];
int brighVal = 255;

int x,y,n;
int p=1;
boolean dStatus = true;

int DisplayMode;
int PrevDisplayMode;

// EEPROM ADDRESS FOR DATA STORAGE.
const int ADD_BRIGHTNESS = 0;
const int ADD_Mode = 1;
const int ADD_Width = 2;
const int ADD_Height  = 3;
const int ADD_dStatus =  4 ;
const int ADD_JsonDataSize = 5;
const int ADD_JsonData = 10;

int DISPLAYS_ACROSS = EEPROM.read(ADD_Width);
int DISPLAYS_DOWN = EEPROM.read(ADD_Height);

DMD dmd(DISPLAYS_ACROSS, DISPLAYS_DOWN);

String LoadCustomEEPROM() {
  String sLoadCustom;
  int BufferSize = EEPROM.read(ADD_JsonDataSize) + 8;
  char myString[BufferSize];
  for (int i = ADD_JsonData ; i < BufferSize; i++)
  {
    myString[i - ADD_JsonData] = EEPROM.read(i);
  }
  for (int k = 0; k < BufferSize - 10; k++) {
    sLoadCustom += String(myString[k]);
  }
  return sLoadCustom;
}

JsonObject& getJsonroot(String jsond) {
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.parseObject(jsond);
  return root;
}
void ScanDMD()
{
  dmd.scanDisplayBySPI();
}
void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);
  ///////////////////////////////////reading all serial data////////////////////////
  Serial.println("......................Setup start...........................");
  Serial.print("Display WIDTH:");
  Serial.println(EEPROM.read(ADD_Width));
  Serial.print("Display Height:");
  Serial.println(EEPROM.read(ADD_Height));
  Serial.println(".................................................");
  Serial.print("brightness");
  Serial.println(EEPROM.read(ADD_BRIGHTNESS));
  Serial.println(".................................................");
  Serial.print("Mode");
  Serial.println( EEPROM.read(ADD_Mode));
  Serial.println(".................................................");
  Serial.print("DStatus");
  Serial.println( EEPROM.read(ADD_dStatus));
  Serial.println("......................end...........................");

  Timer1.initialize(3998);           //period in microseconds to call ScanDMD. Anything longer than 5000 (5ms) and you can see flicker.
  Timer1.attachInterrupt( ScanDMD );   //attach the Timer1 interrupt to ScanDMD which goes to dmd.scanDisplayBySPI()
  dmd.clearScreen( true );
  Wire.begin();
  RTC.begin();
  if (! RTC.isrunning()) {
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
  if (EEPROM.read(ADD_Width) >= 1) {
    p++;
  }
  //Read the data from eeprom
  if (EEPROM.read(ADD_BRIGHTNESS) > 0 ) {
    brighVal = EEPROM.read(ADD_BRIGHTNESS);
  }
  dmd.setBrightness(brighVal);
  if (EEPROM.read(ADD_Mode) >= 1) {
    DisplayMode = EEPROM.read(ADD_Mode);
    // restore Custom data
    if (DisplayMode == 2)
    {
      custData = LoadCustomEEPROM();
      JsonObject& roots = getJsonroot(custData);
      if (roots.containsKey("Custom")) {
        JsonArray& customData = roots["Custom"];
        cDataSize = customData.size();
        for (int i = 0; i < cDataSize; i++) {
          customJson[i] = customData[i];
          // Serial.println(customJson[i]);
        }
      }
    }
  }
}
void(* resetFunc) (void) = 0; //declare reset function @ address 0

void loop() {
  if (p == 1) {
    EEPROM.update(ADD_Width, 1);
    EEPROM.update(ADD_Height, 1);
    delay(250);
    resetFunc();
  }
  p++;
  if (dStatus) {
    //    Serial.println("......................................");
    //    Serial.print("displayMode");
    //    Serial.println(DisplayMode);
    //    Serial.println("......................................");
    switch (DisplayMode) {
      case 0 :
        PrevDisplayMode =  DisplayMode;
        RunDefault();
        break;
      case 1 :
        PrevDisplayMode =  DisplayMode;
        Datetime(1);
        delay(500);
        Temperature(1);
        delay(500);
        Datetime(0);
        delay(500);
        break;
      case 2 :
        PrevDisplayMode =  DisplayMode;

        CustomTrigger = false;
        for (int i = 0; i < cDataSize ; i++) {
          if (customJson[i] == 1) //BLINK SINGLE
          {
            JsonObject& root = getJsonroot(custData);
            JsonArray& data1 = root["DATA"][i];
            BS(data1[0], data1[1], data1[2], data1[3], data1[4], data1[5], data1[6]);
          }
          if (customJson[i] == 2) // BLINK DOUBLE
          {
            JsonObject& root = getJsonroot(custData);
            JsonArray& data1 = root["DATA"][i];
            BD(data1[0], data1[1], data1[2], data1[3], data1[4], data1[5], data1[6], data1[7], data1[8], data1[9], data1[10]);
          }
          if (customJson[i] == 3) // SIMPLE MESSAGE
          {
            JsonObject& root = getJsonroot(custData);
            JsonArray& data1 = root["DATA"][i];
            SimpleMessage(data1[0], data1[1], data1[2], data1[3], data1[4], data1[5]);
          }
          if (customJson[i] == 4) //SCROLL
          {
            JsonObject& root = getJsonroot(custData);
            JsonArray& data1 = root["DATA"][i];
            scroll(data1[0], data1[1], data1[2], data1[3], data1[4]);
          }
          if (customJson[i] == 5) //SPLIT
          {
            JsonObject& root = getJsonroot(custData);
            JsonArray& data1 = root["DATA"][i];
            Split(data1[0], data1[1], data1[2], data1[3], data1[4], data1[5], data1[6], data1[7], data1[8], data1[9]);
          }
          if (customJson[i] == 6) //TEMPERATURE
          {
            JsonObject& root = getJsonroot(custData);
            JsonArray& data1 = root["DATA"][i];
            Temperature(data1[0]);
          }
          if (customJson[i] == 7)  //counter
          {
            JsonObject& root = getJsonroot(custData);
            JsonArray& data1 = root["DATA"][i];
            Counter(data1[0]);
          }
          if (customJson[i] == 8)  //DATE TIME
          {
            JsonObject& root = getJsonroot(custData);
            JsonArray& data1 = root["DATA"][i];
            Datetime(data1[0]);
          }
        }
        break;
      default:
        scroll("WELCOME IoTrix", 14, 0, 30, 7);
        break;
    }
  }
  else {
    //Serial.println("clearing screen");
    dmd.clearScreen(true);
  }
}


//
void serialEvent1() {
  while (Serial1.available()) {
    DynamicJsonBuffer jsonBuffer1;
    Jsondata = Serial1.readString();
    Serial.println(Jsondata);
    JsonObject& roota = jsonBuffer1.parseObject(Jsondata);
    // JsonObject& roota = getJsonroot(Jsondata);
    if (roota.containsKey("Custom")) {
      JsonArray& customData = roota["Custom"];
      CustomTrigger = true;
      custData = Jsondata;
      Jsondata = "";
      Serial.println("..................custom data....................");
      cDataSize = customData.size();
      //      Serial.println("..........data size........");
      //      Serial.println(cDataSize);
      //      Serial.println("..........end........");
      //      Serial.print("Display WiDTH:");
      //      Serial.println(EEPROM.read(ADD_Width));
      //      Serial.print("Display Height:");
      //      Serial.println(EEPROM.read(ADD_Height));
      //      Serial.println("..........for loop 2........");

      for (int i = 0; i < cDataSize; i++) {
        customJson[i] = customData[i];
        //  Serial.println(customJson[i]);
      }
      // Serial.println("..........end 2........");
      DisplayMode = 2;
      EEPROM.update(ADD_Mode, 2);
      //  Serial.println(DisplayMode);
      // Serial.println("............end custom.......");
      SaveCustomEEprom(custData, custData.length());
      delay(250);
    }
    delay(250);
    if (roota.containsKey("DStatus")) {
      if (!CustomTrigger) {
        // Serial.println(".................DStatus....................");
        dStatus = roota["DStatus"];
        EEPROM.update(ADD_dStatus, dStatus);
        if (dStatus == false) {
          dmd.clearScreen(true);
          break;
        }
      } else {
        //        Serial.println(".................display mode....................");
        //        Serial.println(DisplayMode);
        //        Serial.println(".....................end..................");
      }
    }
    if (roota.containsKey("DSize")) {
      JsonObject& DSize = roota["DSize"];
      DISPLAYS_ACROSS  = DSize["DWD"];
      DISPLAYS_DOWN = DSize["DHT"];
      //      Serial.print("Display WiDTH:");
      //      Serial.println(DISPLAYS_ACROSS);
      //      Serial.print("Display Height:");
      //      Serial.println(DISPLAYS_DOWN);
      EEPROM.update(ADD_Width, DISPLAYS_ACROSS);
      EEPROM.update(ADD_Height, DISPLAYS_DOWN);
      //      Serial.println(".................display mode dsize....................");
      //      Serial.println(DisplayMode);
      //      Serial.println(".....................end..................");
      delay(250);
      resetFunc();
    }

    if (roota.containsKey("MxMode")) {
      int x = roota["MxMode"];
      switch (x) {
        case 0:
          DisplayMode = 0;                 //default mode
          EEPROM.update(ADD_Mode, 0);
          break;
        case 1 :
          DisplayMode = 1;                     //time/Date mode
          EEPROM.update(ADD_Mode, 1);
          break;
        case 2:
          DisplayMode = 2;            //custom mode
          EEPROM.update(ADD_Mode, 2);
          break;
      }
      //      Serial.println(".................display mode MXmode....................");
      //      Serial.println(DisplayMode);
      //      Serial.println(".....................end..................");
    }

    if (roota.containsKey("BiM")) {
      brighVal = roota["BiM"];
      switch (brighVal) {
        case 0:
          brighVal = 2;
          break;
        case 1:
          brighVal = 10;
          break;
        case 2 :
          brighVal = 25;
          break;
        case 3 :
          brighVal = 200;
          break;
      }
      EEPROM.update(ADD_BRIGHTNESS, brighVal);
      dmd.setBrightness(brighVal);
      //      Serial.println(".................display mode brighVal....................");
      //      Serial.println(DisplayMode);
      //      Serial.println(".....................end..................");
    }
  }
}
////
////Writing data to EERPOM
void SaveCustomEEprom(String customData, int eepromMax) {
  // Serial.println("...........Saving data................");
  EEPROM.put(ADD_JsonDataSize, eepromMax);
  char configEEPROM[eepromMax + 8 ];
  customData.toCharArray(configEEPROM, sizeof(configEEPROM));
  //WRITE
  for (int j = ADD_JsonData ; j <= eepromMax + 8 ; j++) {
    EEPROM.put(j, configEEPROM[j - ADD_JsonData]);
  }
}

////
void RunDefault() {
  scroll("+ METRO CHEMIST + Experienced Since 1983 +", 43, 0, 40, 7);
  Temperature(1); //temp
  Datetime(0); //time
  Split("METRO", 5, "+CHEMIST+", 10, 1, 0, 2, 50, 0, 2);
  scroll("+ Skin Care + Baby Care + Vitamins + Ayurvedic + ", 50, 0, 30, 7);
  scroll("+GENERIC DRUGS+ ", 15, 0, 30, 7);
  Datetime(0); //time
  Split("METRO", 5, "HOME DELIVERY:28463547", 23, 1, 0, 2, 50, 0, 2);
}

//Scrolling Single MSG
void scroll(const char *Msg, int len, int direc, int spd, int MFont) {
  dmd.clearScreen(true);
  setUpFont(MFont);
  dmd.drawMarquee(Msg, len, 32, 0);
  long start = millis();
  long timer = start;
  boolean ret = false;
  while (!ret) {
    serialEvent1();
    if ((DisplayMode != PrevDisplayMode) ||  CustomTrigger == true || dStatus == false) {
      break;
    }
    if ((timer + spd) < millis()) {
      ret = dmd.stepMarquee(-1, 0);
      timer = millis();
    }
  }
}

//Split
void Split(const char *message1, int ML1, const char *message2, int ML2, int xdis, int ydis, int count , int M1speed, int mode, int MFont) {
  dmd.clearScreen(true);
  setUpFont(MFont);
  boolean ret = false;
  unsigned long time;
  if (mode == 0 && ((DisplayMode == PrevDisplayMode) ||  CustomTrigger == false || dStatus == false)) {
    dmd.drawString(xdis, ydis, message1, ML1, GRAPHICS_NORMAL); // the stationary string
    dmd.drawMarquee(message2, ML2, 32, 8); // set up the marquee
    time = millis();
    n = 0;
    while (n < count) {
      while (!ret) {
        serialEvent1();
        if ((DisplayMode != PrevDisplayMode) ||  CustomTrigger == true || dStatus == false) {
          break;
        }
        if ((time + M1speed) < millis()) {
          ret = dmd.stepSplitMarquee(8, 15); // parameters are the top & bottom rows to be scrolled
          time = millis();
        }
      }
      ret = false;
      n++ ;
    }
  }
  else if (mode == 1 && (DisplayMode == PrevDisplayMode)) {
    dmd.drawString(xdis, ydis, message1, ML1, GRAPHICS_NORMAL); // the stationary string
    dmd.drawMarquee(message2, ML2, 32, 0); // set up the marquee
    time = millis();
    n = 0;
    while (n < count) {
      while (!ret) {
        serialEvent1();
        if ((DisplayMode != PrevDisplayMode) ||  CustomTrigger == true || dStatus == false) {
          break;
        }
        if ((time + M1speed) < millis()) {
          ret = dmd.stepSplitMarquee(0, 7); // parameters are the top & bottom rows to be scrolled
          time = millis();
        }
      }
      ret = false;
      n++;
    }
  }
  else if (mode == 2  && (DisplayMode == PrevDisplayMode)) {
    dmd.drawString(0, -4, "vvvvvvvvvvv", 11, GRAPHICS_NORMAL); // note the position is above a single DMD so
    dmd.drawString(0, 13, "^^^^^^^^^^^", 11, GRAPHICS_NORMAL); // and this is too far down a single DMD so
    dmd.drawMarquee(message2, ML2, 32, 5);
    time = millis();
    n = 0;
    while (n < count) {
      while (!ret) {
        serialEvent1();
        if ((DisplayMode != PrevDisplayMode) ||  CustomTrigger == true || dStatus == false) {
          break;
        }
        if ((time + M1speed) < millis()) {
          ret = dmd.stepSplitMarquee(5, 11); // parameters are the top & bottom rows to be scrolled
          time = millis();
        }
      }
      ret = false;
      n++;
    }
  }
}

//Blink Single
void BS(char *Msg, int len , int xdir, int ydir, int count, int spd, int MFont) {
  dmd.clearScreen(true);
  setUpFont(MFont);
  for (int i = 0; i < count; i++) {
    for (byte x = 0; x < DISPLAYS_ACROSS; x++) {
      for (byte y = 0; y < DISPLAYS_DOWN; y++) {
        serialEvent1();
        if ((DisplayMode != PrevDisplayMode) ||  CustomTrigger == true || dStatus == false) {
          break;
        }
        dmd.drawString(xdir + (32 * x), ydir - 1 + (16 * y), Msg, len, GRAPHICS_OR );
        delay(spd);
        dmd.drawString(xdir + (32 * x), ydir - 1 + (16 * y), Msg, len, GRAPHICS_NOR );
        delay(spd);
      }
    }
  }
}

//BLINK DOUBLE
void BD(const char *m1, int l1, const char *m2, int l2, int xdir1, int xdir2, int ydir1, int ydir2, int count, int spd, int MFont) {
  dmd.clearScreen(true);
  setUpFont(MFont);
  for (int i = 0; i < count; i++) {
    for (byte x = 0; x < DISPLAYS_ACROSS; x++) {
      for (byte y = 0; y < DISPLAYS_DOWN; y++) {
        serialEvent1();
        if ((DisplayMode != PrevDisplayMode) ||  CustomTrigger == true || dStatus == false) {
          break;
        }
        dmd.drawString(xdir1 + (32 * x), ydir1 - 1 + (16 * y), m1, l1, GRAPHICS_OR );
        dmd.drawString(xdir2 + (32 * x), ydir2 - 1 + (16 * y), m2, l2, GRAPHICS_OR );
        delay(spd);
        dmd.drawString(xdir1 + (32 * x), ydir1 - 1 + (16 * y), m1, l1, GRAPHICS_NOR );
        dmd.drawString(xdir2 + (32 * x), ydir2 - 1 + (16 * y), m2, l2, GRAPHICS_NOR );
        delay(spd);
      }
    }
  }
}

//Single Message
void SimpleMessage(const char *Msg , int len, int x, int y, int TC, int MFont) {
  dmd.clearScreen( true );
  setUpFont(MFont);
  dmd.drawString(x - 1, y - 1, Msg, len, GRAPHICS_NORMAL);
  serialEvent1();
  if (DisplayMode == PrevDisplayMode ) {
    delay(TC);
  }
}

//Temperature
void Temperature(int mode) {
  byte i;
  byte present = 0;
  byte type_s;
  byte data[12];
  byte addr[8];
  float celsius, fahrenheit;
  ds.search(addr);
  delay(250);
  ds.reset();
  ds.search(addr);
  ds.select(addr);
  ds.write(0x44, 1);        // start conversion, with parasite power on at the end
  delay(750);     // maybe 750ms is enough for correct start up temp, maybe not
  present = ds.reset();   // we might do a ds.depower() here, but the reset will take care of it.
  ds.select(addr);
  ds.write(0xBE);         // Read Scratchpad
  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
  }
  int16_t raw = (data[1] << 8) | data[0];
  if (type_s) {
    raw = raw << 3; // 9 bit resolution default
    if (data[7] == 0x10) {
      // "count remain" gives full 12 bit resolution
      raw = (raw & 0xFFF0) + 12 - data[6];
    }
  } else {
    byte cfg = (data[4] & 0x60);
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms
  }
  celsius = raw / 16.0;
  fahrenheit = celsius * 1.8 + 32.0;
  celsius = celsius + 1; //fine tune temp here if out
  dmd.clearScreen(true);
  float t = celsius; // Temp Fine tune.
  // Serial.println(t);
  char str2_buf[10];
  switch (mode) {
    case 0:
      snprintf(str2_buf, 20, "Temperature %d.%d" "*C", (int)t, (int)(t * 100) - (int)t * 100); // string with degree C
      scroll(str2_buf, 20, 0, 30, 7); //scrolling *C
      break;
    case 1:
      dmd.selectFont(SystemFont5x7);
      dmd.drawString( 5 + (32 * x), 0 + (16 * y), "TEMP", 4, GRAPHICS_NORMAL);
      //snprintf(str2_buf,4, "%d.%d ", (int)t, (int)(t * 100) - (int)t * 100); // string with degree C
      // Serial.println(str2_buf);
      floatToString(str2_buf, t, 2);
      dmd.drawString(2 + (32 * x), 8 + (16 * y), str2_buf, 5, GRAPHICS_NORMAL);
      break;
    case 2:
      snprintf(str2_buf, 20, "Temperature %d.%d" "*F", (int)fahrenheit, (int)(fahrenheit * 100) - (int)fahrenheit * 100); // string with degree F
      scroll(str2_buf, 20, 0, 30, 7); //Scrolling *F
      break;
  }
  delay(1000);
}

//Counter
void Counter(int mode) {
  dmd.selectFont(Arial_Black_16);
  int j = 0;
  switch (mode) {
    case 0:
      j = 10;
      break;
    case 1:
      j = 15;
      break;
    case 2:
      j = 30;
      break;
    case 3:
      j = 45;
      break;
    case 4:
      j = 60;
      break;
    case 5:
      j = 120;
      break;
  }
  for (int counter = j; counter > 0; counter--) {
    dmd.clearScreen(true);
    serialEvent1();
    if ((DisplayMode != PrevDisplayMode) ||  CustomTrigger == true  || dStatus == false) {
      break;
    }
    if (counter > 99) dmd.drawChar(2, 1, '0' + ((counter % 1000) / 100), GRAPHICS_NORMAL); // hundreds
    if (counter > 9) dmd.drawChar(10, 1, '0' + ((counter % 100) / 10), GRAPHICS_NORMAL);  // tens
    dmd.drawChar(20, 1, '0' + (counter % 10), GRAPHICS_NORMAL);                           // units
    delay(1000);
  }
  // stripe chaser
  for (byte  b = 0 ; b < 20 ; b++ )
  {
    serialEvent1();
    if ((DisplayMode != PrevDisplayMode) ||  CustomTrigger == true  || dStatus == false) {
      break;
    }
    dmd.drawTestPattern( (b & 1) + PATTERN_STRIPE_0 );
    delay( 200 );
  }
}

//Date and time
void Datetime(int mode) {
  DateTime now = RTC.now();
  switch (mode) {
    case 0:      //Time
      sprintf(Timebuf, "%02d:%02d", now.hour(), now.minute());
      SimpleMessage(Timebuf, 10, 2, 0, 5000, 8);
      break;
    case 1:
      sprintf(Datebuf, "%02d/%02d/%02d", now.day(), now.month(), now.year());
      scroll(Datebuf, 10, 0, 30, 7);
      break;
    case 2: //day
      scroll((daysOfTheWeek[now.dayOfTheWeek()]), 10, 0, 30, 7);
      break;
  }
}

//Setting fonts
void setUpFont(int Fmode) {
  switch (Fmode) {
    case 0:
      dmd.selectFont(Droid_Sans_12);
      break;
    case 1:
      dmd.selectFont(Droid_Sans_16);
      break;
    case 2:
      dmd.selectFont(SystemFont5x7);
      break;
    case 3:
      dmd.selectFont(comicSans_14);
      break;
    case 4 :
      dmd.selectFont(seogeScript);
      break;
    case 5:
      dmd.selectFont(arialRounded_9);
      break;
    case 6:
      dmd.selectFont(arial_11);
      break;
    case 7:
      dmd.selectFont(Arial_Black_16);
      break;
    case 8:
      dmd.selectFont(OCRA);
      break;
  }
}
